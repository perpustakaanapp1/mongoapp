# MongoDB APP

## Simple Backend Microservice Online Shop Dengan MongoDB
---

aplikasi ini hanya berisi table user dan produk dengan menggunakan MongoDb, FastAPI, dan ODM Mongoengine sebagai penulisan kodenya. 

---
## Langkah Penggunaan

1. User
    * Menampilkan seluruh data user dengan cara mengakses GET pada postman *** http://127.0.0.1:8000/user ***
    * Insert data user dengan cara mengakses POST pada postman dan *** http://127.0.0.1:8000/user/insert *** dan inputkan data yang akan dimasukkan, sebagai contoh {"email": "cr7@gmail.com", "password": "123", "nama_lengkap": "Cristiano Roanldo", "gender": "Laki - Laki", "no_telp": "082131233"}
    * Update data user dengan cara mengakses POST pada postman dan *** http://127.0.0.1:8000/user/update *** dan inputkan data baru yang akan dimasukkan berdasarkan email, sebagai contoh { "email": "cr7@gmail.com", "password" : "123", "nama_lengkap": "Lionel Messi", "gender": "Laki - Laki", "no_telp": "082131233"}
    * delete data user dengan cara mengakses POST pada postman dan *** http://127.0.0.1:8000/user/delete *** berdasarkan email, sebagai contoh { "email": "cr7@gmail.com"}
    * Menampilkan data user by email dengan mengakses POST pada postman dan *** http://127.0.0.1:8000/user/email *** dan menginputkan email, sebagai contoh { "email": "Shroud@gmail.com"}

2. Produk
    * Menampilkan seluruh data produk dengan cara mengakses POST pada postman *** http://127.0.0.1:8000/produk ***
    * Insert data produk dengan cara mengakses POST pada postman *** http://127.0.0.1:8000/produk/insert *** dan menginputkan data yang mau dimasukkan, sebagai contoh : { "nama" : "Logitech M90", "price" : "60000", "nama_toko" : "Logitech", "stock" : 22 }
    * Update data produk dengan cara mengases POST pada postman *** http://127.0.0.1:8000/produk/update *** by nama, sehingga input nama tidak dapat diubah. sebagai contoh : { "nama" : "Logitech M90", "price" : "160000", "nama_toko" : "Logitech NEW STORE", "stock" : 4 }
    * delete data produk dengan cara mengakses POST pada postman *** http://127.0.0.1:8000/produk/delete *** by nama, sebagai contoh : {"nama":"Logitech M90"}
