from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument
from mongoengine.fields import IntField

connection = connect(db="final_project", host="localhost", port=27017)
if connection:
    print("connection success")

class Produk(Document):
    nama = StringField()
    price = StringField()
    nama_toko = StringField()
    stock = IntField()

class CRUD_produk():
    def showallproduk(self):
        try:
            result = []
            for items in Produk.objects:
                produk = {
                    "nama" : items.nama,
                    "price" : items.price,
                    "nama_toko" : items.nama_toko,
                    "stock" : items.stock
                }
                result.append(produk)
            return result
        except Exception as e:
            print(e)
    def addproduk(self, **param):
        try:
            Produk(**param).save()
        except Exception as e:
            print(e)
    def showprodukbynamatoko(self, param):
        try:
            result = []
            for items in Produk.objects(nama_toko = param):
                produk = {
                    "nama" : items.nama,
                    "price" : items.price,
                    "nama_toko" : items.nama_toko,
                    "stock" : items.stock
                }
                result.append(produk)
            return result
        except Exception as e:
            print(e)
    def deleteprodukbynamaproduk(self, param):
        try:
            doc = Produk.objects(nama = param).first()
            doc.delete()
        except Exception as e:
            print(e)
    
    def updateproduk(self, param):
        try:
            doc = Produk.objects(nama = param['nama']).first()
            doc.price = param['price']
            doc.nama_toko = param['nama_toko']
            doc.stock = param['stock']
            doc.save()
        except Exception as e:
            print(e)