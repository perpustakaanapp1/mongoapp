from fastapi import APIRouter
from produk_api import *

router = APIRouter()

@router.post('/produk/insert')
def addsomeproduk(param:dict):
    result = insert_produk(**param)
    return result

@router.post('/produk')
def showproduk():
    return show_all_produk()

@router.post('/produk/nama_toko')
def lihatprodukpertoko(param:dict):
    result = show_produk_by_nama_toko(param['nama_toko'])
    return result

@router.post('/produk/delete')
def deletproduk(param:dict):
    result = delete_produk_by_nama_produk(param['nama'])
    return result

@router.post('/produk/update')
def updatesomeproduk(param:dict):
    result = update_produk_by_id(param)
    return result