from fastapi import APIRouter
from user_api import *

router = APIRouter()

@router.post('/user/insert')
async def insertsomeuser(param:dict):
    result = insert_user(**param)
    return result

@router.get('/user')
async def showalluser():
    return show_all()

@router.post('/user/delete')
async def deleteuserbyemail(param:dict):
    result = delete_user_by_email(param['email'])
    return result

@router.post('/user/update')
async def updateuserbyemail(param:dict):
    result = update_user_by_email(param)
    return result

@router.post('/user/email')
async def showuserbyemail(param:dict):
    return show_user_by_email(param['email'])